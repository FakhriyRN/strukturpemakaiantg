package customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

public class TextRoboto extends AppCompatTextView {

    public TextRoboto(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextRoboto(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextRoboto(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Nexa-Bold.otf");
            setTypeface(tf);
        }
    }

}