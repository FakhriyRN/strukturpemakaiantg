package com.mamad.sisteminformasi.Activity;

import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mamad.sisteminformasi.Adapter.FlightAdapter;
import com.mamad.sisteminformasi.Object.Contact;
import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.Object.Gradient;
import com.mamad.sisteminformasi.R;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends CustomBarActivity {

    public static List<Flight> flightList;
    private FlightAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_main);

        setLayout(R.id.customBar);
        setTitle("Motor List");

        flightList = new ArrayList<>();

        initYamaha();
        initHonda();
        initKawasaki();
        initDucati();
        initBenelli();

        fillData();
    }

    private void initYamaha() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Call Centre ( Indonesia )", "+021 2457 5555"));
        contacts.add(new Contact("Dealer Yamaha ( Surabaya )", "(031) 5996215"));

        flightList.add(new Flight(
                "Yamaha",
                "Japan",
                "12 Oktober 1897",
                125,
                "Yamaha Corporation s a Japanese multinational corporation and conglomerate with a very wide range of products and services, predominantly musical instruments, electronics and power sports equipment. It is one of the constituents of Nikkei 225 and is the world's largest piano manufacturing company. The former motorcycle division became independent from the main company in 1955, forming Yamaha Motor Co., Ltd, although Yamaha Corporation is still the largest shareholder.",
                "singapore",
                "https://yamaha-motor.com/",
                new LatLng(-7.305473, 112.762254),
                contacts,
                new Gradient(getColorValue(R.color.darkOne), getColorValue(R.color.lightOne))
        ));
    }

    private void initHonda() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Call Center", "0811-9-500-989"));

        flightList.add(new Flight(
                "Honda",
                "Japan",
                "24 September 1948",
                225,
                "Honda Motor Company, Ltd is a Japanese public multinational conglomerate corporation primarily known as a manufacturer of automobiles, motorcycles, and power equipment. Honda has been the world's largest motorcycle manufacturer since 1959,as well as the world's largest manufacturer of internal combustion engines measured by volume, producing more than 14 million internal combustion engines each year.Honda became the second-largest Japanese automobile manufacturer in 2001.",
                "qatar",
                "https://global.honda/",
                new LatLng(-7.291482, 112.755926),
                contacts,
                new Gradient(getColorValue(R.color.darkTwo), getColorValue(R.color.lightTwo))
        ));
    }

    private void initKawasaki() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Dealer ( Surabaya )", "(031) 5925554"));
//        contacts.add(new Contact("Baggage Complaint ( Indonesia )", "+62215507096"));

        flightList.add(new Flight(
                "Kawasaki",
                "Japan",
                "25 October 1985",
                269,
                "Kawasaki Heavy Industries Ltd.is a Japanese public multinational corporation primarily known as a manufacturer of motorcycles, heavy equipment, aerospace and defense equipment, rolling stock and ships. It is also active in the production of industrial robots, gas turbines, boilers and other industrial products. The company is named after its founder Shōzō Kawasaki, and has dual headquarters in Chūō-ku, Kobe and Minato, Tokyo.\n" +
                        "\n" +
                        "KHI is known as one of the three major heavy industrial manufacturers of Japan, alongside Mitsubishi Heavy Industries and IHI.\n" +
                        "\n" +
                        "Prior to World War II, KHI was part of the Kobe Kawasaki zaibatsu, which included Kawasaki Steel and Kawasaki Kisen. After the war, KHI became part of the DKB Group (keiretsu).",
                "emirates",
                "https://kawasaki-motor.co.id/",
                new LatLng(-7.295689, 112.781982),
                contacts,
                new Gradient(getColorValue(R.color.darkOne), getColorValue(R.color.lightOne))
        ));
    }

    private void initDucati() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Dealer (Malang) ", "(021) 71792049"));
//        contacts.add(new Contact("Call Center 2", "+622123519999"));

        flightList.add(new Flight(
                "Ducati",
                "Italia",
                "1926 ",
                139,
                "Ducati Motor Holding S.p.A. is the motorcycle-manufacturing division of Italian company Ducati, headquartered in Bologna, Italy. The company is owned by German automotive manufacturer Audi through its Italian subsidiary Lamborghini, which is in turn owned by the Volkswagen Group",
                "garuda",
                "https://www.ducati.com/ww/en/home",
                new LatLng(-7.980616, 112.653893),
                contacts,
                new Gradient(getColorValue(R.color.darkTwo), getColorValue(R.color.lightTwo))
        ));
    }

    private void initBenelli() {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Dealer (Surabaya) ", "(031) 5351352"));
//        contacts.add(new Contact("Fortune Wings Club", "+86898950717"));
//        contacts.add(new Contact("Cargo", "+861056170030"));

        flightList.add(new Flight(
                "Benelli Motor",
                "Italia",
                "1911",
                239,
                "Established in 1911, Benelli is one of the oldest Italian motorcycle manufacturers.[1] It once manufactured shotguns, although this part of the business is now a separate company.",
                "hainan",
                "https://indonesia.benelli.com/",
                new LatLng(-7.255912, 112.729304),
                contacts,
                new Gradient(getColorValue(R.color.darkOne), getColorValue(R.color.lightOne))
        ));
    }

    private void fillData() {
        adapter = new FlightAdapter(this, flightList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private int getColorValue(int id) {
        return ContextCompat.getColor(this, id);
    }
}
