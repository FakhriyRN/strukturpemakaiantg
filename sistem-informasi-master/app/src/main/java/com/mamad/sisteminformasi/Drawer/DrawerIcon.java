package com.mamad.sisteminformasi.Drawer;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;

public class DrawerIcon {

    Context mContext;

    private DrawerIcon(Context ctx) throws ClassCastException {
        this.mContext = ctx;
    }

    public static LayerDrawable getIcon(int darkColor, int lightColor) {
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,
                new int[] { darkColor, lightColor });

        gradientDrawable.setCornerRadius(20f);

        Drawable[] layers = { gradientDrawable };
        LayerDrawable layerDrawable = new LayerDrawable(layers);

        layerDrawable.setLayerInset(0, 10, 10, 10, 10);

        return layerDrawable;
    }
}
