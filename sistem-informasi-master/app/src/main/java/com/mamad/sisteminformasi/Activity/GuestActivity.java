package com.mamad.sisteminformasi.Activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mamad.sisteminformasi.Adapter.CommentAdapter;
import com.mamad.sisteminformasi.Helper.DatabaseHelper;
import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;

public class GuestActivity extends CustomBarActivity implements View.OnClickListener {

    private DatabaseHelper databaseHelper;
    private CommentAdapter adapter;
    private Flight flight;

    RecyclerView recyclerGuest;
    EditText editName, editComment;
    ImageButton btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_guest);

        setLayout(R.id.customBar);
        setTitle("Guest Book");
        setHomeEnable();

        flight = getIntent().getParcelableExtra("flight");

        databaseHelper = new DatabaseHelper(this);

        recyclerGuest = findViewById(R.id.recyclerGuest);
        editName = findViewById(R.id.editName);
        editComment = findViewById(R.id.editComment);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        this.fillData();
        this.resetInput();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                String name = editName.getText().toString().trim();
                String comment = editComment.getText().toString().trim();

                if (name.equals("") || comment.equals("")) {
                    showToast("All Field is Required");
                    return;
                }

                databaseHelper.addComment(flight.getName(), name, comment);
                this.fillData();
                this.resetInput();
                this.showToast("Add Comment Success");
                break;
        }
    }

    void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    void fillData() {
        adapter = new CommentAdapter(databaseHelper.getCommentList(flight.getName()));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerGuest.setLayoutManager(layoutManager);
        recyclerGuest.setAdapter(adapter);
    }

    void resetInput() {
        editName.setText("");
        editComment.setText("");
    }
}
