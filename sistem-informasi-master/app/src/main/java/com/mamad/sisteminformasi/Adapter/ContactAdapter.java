package com.mamad.sisteminformasi.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mamad.sisteminformasi.Object.Contact;
import com.mamad.sisteminformasi.R;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private Context context;
    private List<Contact> dataList;

    public ContactAdapter(Context context, List<Contact> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.contact_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtContactName.setText(dataList.get(position).getName());
        holder.txtPhone.setText(dataList.get(position).getPhone());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtContactName, txtPhone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtContactName = itemView.findViewById(R.id.txtContactName);
            txtPhone = itemView.findViewById(R.id.txtPhone);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contact contact = dataList.get(getAdapterPosition());
                    context.startActivity(new Intent(Intent.ACTION_CALL,
                            Uri.parse("tel:" + contact.getPhone())));
                }
            });
        }
    }
}
