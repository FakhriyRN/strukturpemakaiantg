package com.mamad.sisteminformasi.Activity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;

public class WebActivity extends CustomBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_web);

        setLayout(R.id.customBar);
        setTitle("Website");
        setHomeEnable();

        final Flight flight = getIntent().getParcelableExtra("flight");

        WebView webView = findViewById(R.id.webview);
        webView.loadUrl(flight.getWebsite());

        // Enable Javascript
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Force links and redirects to open in the WebView instead of in a browser
        webView.setWebViewClient(new WebViewClient());
    }
}
