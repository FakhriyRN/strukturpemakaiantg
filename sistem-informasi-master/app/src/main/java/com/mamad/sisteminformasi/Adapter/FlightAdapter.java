package com.mamad.sisteminformasi.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mamad.sisteminformasi.Drawer.DrawerIcon;
import com.mamad.sisteminformasi.Fragment.BottomSheetFragment;
import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;

import java.util.List;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ViewHolder> {

    private Context context;
    private List<Flight> dataList;

    public FlightAdapter(Context context, List<Flight> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.flight_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Flight flight = dataList.get(position);

        holder.txtName.setText(flight.getName());
        holder.txtCountry.setText(flight.getCountry());
        holder.imgFlight.setBackground(DrawerIcon.getIcon(flight.getColor().getDarkColor(),
                flight.getColor().getLightColor()));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgFlight;
        TextView txtName, txtCountry;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgFlight = itemView.findViewById(R.id.imgFlight);
            txtName = itemView.findViewById(R.id.txtName);
            txtCountry = itemView.findViewById(R.id.txtCountry);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Flight flight = dataList.get(getAdapterPosition());

                    BottomSheetFragment fragment = new BottomSheetFragment(flight);
                    fragment.show(((FragmentActivity) context).getSupportFragmentManager(), "bottom_sheet");
                }
            });
        }
    }
}
