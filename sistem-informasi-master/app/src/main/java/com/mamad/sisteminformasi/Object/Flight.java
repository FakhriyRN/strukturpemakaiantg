package com.mamad.sisteminformasi.Object;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class Flight implements Parcelable {

    private String name;
    private String country;
    private String founded;
    private Integer fleet_size;
    private String description;
    private String image;
    private String website;
    private LatLng location;
    private List<Contact> contacts;
    private Gradient color;

    public Flight(String name, String country, String founded, Integer fleet_size, String description, String image, String website, LatLng location, List<Contact> contacts, Gradient color) {
        this.name = name;
        this.country = country;
        this.founded = founded;
        this.fleet_size = fleet_size;
        this.description = description;
        this.image = image;
        this.website = website;
        this.location = location;
        this.contacts = contacts;
        this.color = color;
    }

    protected Flight(Parcel in) {
        name = in.readString();
        country = in.readString();
        founded = in.readString();
        if (in.readByte() == 0) {
            fleet_size = null;
        } else {
            fleet_size = in.readInt();
        }
        description = in.readString();
        image = in.readString();
        website = in.readString();
        location = in.readParcelable(LatLng.class.getClassLoader());
        contacts = in.createTypedArrayList(Contact.CREATOR);
        color = in.readParcelable(Gradient.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(country);
        dest.writeString(founded);
        if (fleet_size == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(fleet_size);
        }
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(website);
        dest.writeParcelable(location, flags);
        dest.writeTypedList(contacts);
        dest.writeParcelable(color, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Flight> CREATOR = new Creator<Flight>() {
        @Override
        public Flight createFromParcel(Parcel in) {
            return new Flight(in);
        }

        @Override
        public Flight[] newArray(int size) {
            return new Flight[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFounded() {
        return founded;
    }

    public void setFounded(String founded) {
        this.founded = founded;
    }

    public Integer getFleet_size() {
        return fleet_size;
    }

    public void setFleet_size(Integer fleet_size) {
        this.fleet_size = fleet_size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public Gradient getColor() {
        return color;
    }

    public void setColor(Gradient color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", founded='" + founded + '\'' +
                ", fleet_size=" + fleet_size +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", website='" + website + '\'' +
                ", location=" + location +
                ", contacts=" + contacts +
                ", color=" + color +
                '}';
    }
}
