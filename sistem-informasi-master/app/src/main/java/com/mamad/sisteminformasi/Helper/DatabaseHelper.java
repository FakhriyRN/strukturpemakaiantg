package com.mamad.sisteminformasi.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mamad.sisteminformasi.Object.Comment;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "flight_db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_COMMENTS = "comments";
    private static final String KEY_ID = "id";
    private static final String KEY_FIRSTNAME = "type";
    private static final String KEY_SECONDNAME = "name";
    private static final String KEY_THIRDNAME = "comment";

    /*CREATE TABLE students ( id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, phone_number TEXT......);*/

    private static final String CREATE_TABLE_COMMENTS = "CREATE TABLE "
            + TABLE_COMMENTS + "(" + KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_FIRSTNAME + " TEXT,"
            + KEY_SECONDNAME + " TEXT,"
            + KEY_THIRDNAME + " TEXT);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        Log.d("table", CREATE_TABLE_COMMENTS);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_COMMENTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + TABLE_COMMENTS + "'");
        onCreate(db);
    }

    public long addComment(String type, String name, String comment) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(KEY_FIRSTNAME, type);
        values.put(KEY_SECONDNAME, name);
        values.put(KEY_THIRDNAME, comment);

        // insert row in students table
        return db.insert(TABLE_COMMENTS, null, values);
    }

    public List<Comment> getCommentList(String typeName) {
        List<Comment> dataList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_COMMENTS + " WHERE type = '" + typeName +"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                String type = cursor.getString(cursor.getColumnIndex(KEY_FIRSTNAME));
                String name = cursor.getString(cursor.getColumnIndex(KEY_SECONDNAME));
                String comment = cursor.getString(cursor.getColumnIndex(KEY_THIRDNAME));
                dataList.add(new Comment(type, name, comment));
            }
            while (cursor.moveToNext());

            Log.d("array", dataList.toString());
        }

        return dataList;
    }
}
