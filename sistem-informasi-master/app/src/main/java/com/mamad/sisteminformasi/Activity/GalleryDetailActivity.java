package com.mamad.sisteminformasi.Activity;

import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.ScaleGestureDetector;

import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;
import com.bumptech.glide.Glide;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class GalleryDetailActivity extends CustomBarActivity {

    private PhotoView imageView;
    private ScaleGestureDetector scaleGestureDetector;
    private Matrix matrix = new Matrix();
    private PhotoViewAttacher photoViewAttacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_gallery_detail);

        setLayout(R.id.customBar);
        setTitle("Image Detail");
        setHomeEnable();

        Flight flight = getIntent().getParcelableExtra("flight");

        Integer id = getIntent().getIntExtra("id", -1);
        String info = getIntent().getStringExtra("info");

        imageView = findViewById(R.id.image);
        TypedArray imgs = getResources().obtainTypedArray(this.getResources().getIdentifier(
                flight.getImage() + "_gallery", "array", this.getPackageName()));

        Glide.with(this)
                .load(imgs.getResourceId(id, -1))
                .into(imageView);
    }
}
