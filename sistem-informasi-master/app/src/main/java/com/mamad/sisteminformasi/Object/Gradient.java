package com.mamad.sisteminformasi.Object;

import android.os.Parcel;
import android.os.Parcelable;

public class Gradient implements Parcelable {

    private Integer darkColor;
    private Integer lightColor;

    public Gradient(Integer darkColor, Integer lightColor) {
        this.darkColor = darkColor;
        this.lightColor = lightColor;
    }

    protected Gradient(Parcel in) {
        if (in.readByte() == 0) {
            darkColor = null;
        } else {
            darkColor = in.readInt();
        }
        if (in.readByte() == 0) {
            lightColor = null;
        } else {
            lightColor = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (darkColor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(darkColor);
        }
        if (lightColor == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(lightColor);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Gradient> CREATOR = new Creator<Gradient>() {
        @Override
        public Gradient createFromParcel(Parcel in) {
            return new Gradient(in);
        }

        @Override
        public Gradient[] newArray(int size) {
            return new Gradient[size];
        }
    };

    public Integer getDarkColor() {
        return darkColor;
    }

    public void setDarkColor(Integer darkColor) {
        this.darkColor = darkColor;
    }

    public Integer getLightColor() {
        return lightColor;
    }

    public void setLightColor(Integer lightColor) {
        this.lightColor = lightColor;
    }

    @Override
    public String toString() {
        return "Gradient{" +
                "darkColor=" + darkColor +
                ", lightColor=" + lightColor +
                '}';
    }
}
