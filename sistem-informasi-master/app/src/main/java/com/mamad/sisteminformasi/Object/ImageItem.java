package com.mamad.sisteminformasi.Object;

public class ImageItem {
    private Integer id;
    private Integer image_resource;
    private String info;

    public ImageItem(Integer id, Integer image_resource, String info) {
        this.id = id;
        this.image_resource = image_resource;
        this.info = info;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getImage_resource() {
        return image_resource;
    }

    public void setImage_resource(Integer image_resource) {
        this.image_resource = image_resource;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
