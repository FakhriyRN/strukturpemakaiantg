package com.mamad.sisteminformasi.Activity;

import android.os.Bundle;

import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends CustomBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_map);

        setLayout(R.id.customBar);
        setTitle("Map");
        setHomeEnable();

        final Flight flight = getIntent().getParcelableExtra("flight");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                mMap.clear();

                CameraPosition googlePlex = CameraPosition.builder()
                        .target(flight.getLocation())
                        .zoom(15)
                        .bearing(0)
                        .tilt(45)
                        .build();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 500, null);

                mMap.addMarker(new MarkerOptions()
                        .position(flight.getLocation())
                        .title(flight.getName()));
            }
        });
    }
}
