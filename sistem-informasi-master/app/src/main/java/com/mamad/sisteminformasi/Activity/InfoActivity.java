package com.mamad.sisteminformasi.Activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;
import com.bumptech.glide.Glide;

public class InfoActivity extends CustomBarActivity {

    ImageView imgDetail;
    TextView txtTitle, txtFounded, txtDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_info);

        setLayout(R.id.customBar);
        setTitle("Information");
        setHomeEnable();

        Flight flight = getIntent().getParcelableExtra("flight");

        imgDetail = findViewById(R.id.imgDetail);
        Glide.with(this)
                .load(this.getResources().getIdentifier(flight.getImage() + "1",
                "drawable", this.getPackageName()))
                .into(imgDetail);

        txtTitle = findViewById(R.id.txtTitle);
        txtTitle.setText(flight.getName());

        txtFounded = findViewById(R.id.txtFounded);
        txtFounded.setText(flight.getFounded());

        txtFounded = findViewById(R.id.txtFounded);
        txtFounded.setText("Founded : " + flight.getFounded());

//        txtFleet = findViewById(R.id.txtFleet);
//        txtFleet.setText("Fleet Size : " + flight.getFleet_size());

        txtDescription = findViewById(R.id.txtDescription);
        txtDescription.setText(flight.getDescription());
    }
}
