package com.mamad.sisteminformasi.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.mamad.sisteminformasi.Adapter.GridViewAdapter;
import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.Object.ImageItem;
import com.mamad.sisteminformasi.R;

import java.util.ArrayList;

public class GalleryActivity extends CustomBarActivity {

    private RelativeLayout view;
    private GridView gridView;
    private GridViewAdapter gridAdapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_gallery);
        context = this;

        setLayout(R.id.customBar);
        setTitle("Gallery");
        setHomeEnable();

        final Flight flight = getIntent().getParcelableExtra("flight");

        gridView = findViewById(R.id.gridGallery);
        gridAdapter = new GridViewAdapter(this, R.layout.gallery_item,
                getData(flight.getImage() + "_gallery"));
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageItem item = (ImageItem) parent.getItemAtPosition(position);

                Intent intent = new Intent(context, GalleryDetailActivity.class);
                intent.putExtra("id", item.getId());
                intent.putExtra("info", item.getInfo());
                intent.putExtra("flight", flight);

                Log.d("Gallery", flight.toString());

                startActivity(intent);
            }
        });
    }

    private ArrayList<ImageItem> getData(String name) {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        @SuppressLint("Recycle") TypedArray imgs = getResources().obtainTypedArray(
                this.getResources().getIdentifier(name, "array", this.getPackageName()));
        @SuppressLint("Recycle") TypedArray infos = getResources().obtainTypedArray(R.array.image_info);

        for (int i = 0; i < imgs.length(); i++) {
            imageItems.add(new ImageItem(i, imgs.getResourceId(i, -1), infos.getString(i)));
        }

        return imageItems;
    }
}
