package com.mamad.sisteminformasi.Activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.mamad.sisteminformasi.Adapter.ContactAdapter;
import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;

public class ContactActivity extends CustomBarActivity {

    private ContactAdapter adapter;
    private Flight flight;

    RecyclerView recyclerContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarGradiant(this);
        setContentView(R.layout.activity_contact);

        setLayout(R.id.customBar);
        setTitle("Contact");
        setHomeEnable();

        checkLocationPermission();

        flight = getIntent().getParcelableExtra("flight");

        recyclerContact = findViewById(R.id.recyclerContact);
        adapter = new ContactAdapter(this, flight.getContacts());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerContact.setLayoutManager(layoutManager);
        recyclerContact.setAdapter(adapter);
    }

    void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    protected void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{ Manifest.permission.CALL_PHONE },
                    200);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 200: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showToast("Permission Granted");
                }
                else {
                    showToast("Permission Denied");
                    finish();
                }
                break;
            }
        }
    }
}
