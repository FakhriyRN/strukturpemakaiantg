package com.mamad.sisteminformasi.Fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mamad.sisteminformasi.Activity.ContactActivity;
import com.mamad.sisteminformasi.Activity.GalleryActivity;
import com.mamad.sisteminformasi.Activity.GuestActivity;
import com.mamad.sisteminformasi.Activity.InfoActivity;
import com.mamad.sisteminformasi.Activity.MapActivity;
import com.mamad.sisteminformasi.Activity.WebActivity;
import com.mamad.sisteminformasi.Object.Flight;
import com.mamad.sisteminformasi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private Flight flight;
    private ImageButton btnInfo, btnContact, btnGallery, btnWebsite, btnMap, btnGuest;

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public BottomSheetFragment(Flight flight) {
        this.flight = flight;

        Log.d("Flight Data BottomSheet", flight.toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_sheet, container, false);

        TextView txtMenu = view.findViewById(R.id.txtMenu);
        txtMenu.setText(flight.getName());

        btnInfo = view.findViewById(R.id.btnInfo);
        btnInfo.setOnClickListener(this);

        btnContact = view.findViewById(R.id.btnContact);
        btnContact.setOnClickListener(this);

        btnGallery = view.findViewById(R.id.btnGallery);
        btnGallery.setOnClickListener(this);

        btnWebsite = view.findViewById(R.id.btnWebsite);
        btnWebsite.setOnClickListener(this);

        btnMap = view.findViewById(R.id.btnMap);
        btnMap.setOnClickListener(this);

        btnGuest = view.findViewById(R.id.btnGuest);
        btnGuest.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.btnInfo:
                intent = new Intent(getContext(), InfoActivity.class);
                break;
            case R.id.btnContact:
                intent = new Intent(getContext(), ContactActivity.class);
                break;
            case R.id.btnGallery:
                intent = new Intent(getContext(), GalleryActivity.class);
                break;
            case R.id.btnWebsite:
                intent = new Intent(getContext(), WebActivity.class);
                break;
            case R.id.btnMap:
                intent = new Intent(getContext(), MapActivity.class);
                break;
            case R.id.btnGuest:
                intent = new Intent(getContext(), GuestActivity.class);
                break;
        }

        intent.putExtra("flight", this.flight);
        startActivity(intent);

        dismiss();
    }
}
