package com.mamad.sisteminformasi.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mamad.sisteminformasi.R;

public class CustomBarActivity extends AppCompatActivity {

    private int layout;
    private TextView textView;
    private boolean backStatus = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.center_toolbar);
        getSupportActionBar().setElevation(0);
    }

    protected void setLayout(int layout) {
        this.layout = layout;
    }

    protected void setTitle(String text) {
        textView = getSupportActionBar().getCustomView().findViewById(this.layout);
        textView.setText(text);
    }

    protected void setHomeEnable() {
        backStatus = true;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TypedArray actionbarSizeTypedArray = this.obtainStyledAttributes(new int[] { android.R.attr.actionBarSize });
        int hel = (int) actionbarSizeTypedArray.getDimension(0, 0);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, hel, 0);
        textView.setLayoutParams(params);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_action_bar);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (backStatus) {
            onBackPressed();
        }

        return super.onSupportNavigateUp();
    }
}
